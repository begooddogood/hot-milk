import fastdom from 'fastdom'

var CursorPointer = (function () {
  var cursorElem
  var cursorTimer
  var winX
  var winY
  var winPad = 20
  var win = window // checks which transform property we should use based on the browser ::  `-webkitTransform` VS `transform`

  var transformProp = window.transformProp || (function () {
    let testEl = document.createElement('div')

    if (testEl.style.transform === null) {
      let vendors = ['Webkit', 'Moz', 'ms']

      for (let vendor in vendors) {
        if (testEl.style[vendors[vendor] + 'Transform'] !== undefined) {
          return vendors[vendor] + 'Transform'
        }
      }
    }

    return 'transform'
  }())

  function init () {
    winX = win.innerWidth - winPad
    winY = win.innerHeight - winPad
    cursorElem = document.querySelector('.follow-cursor')
    if (!cursorElem) return false
    document.body.addEventListener('mousemove', _lockCursor, false)
  }

  function _lockCursor (evt) {
    // Use fastdom to batch the reads
    // and writes with exactly the same
    // code as the 'sync' routine
    fastdom.measure(function () {
      let mouseX = evt.clientX
      let mouseY = evt.clientY
      let cursorPointer = window.getComputedStyle(evt.target)['cursor'] // let cursorClasses = Array.from(evt.target.dataset);

      let hideCursor = false // cursorPointer === 'none'
      let animate = false

      if (mouseX >= winX - winPad || mouseX <= winPad || mouseY >= winY - winPad || mouseY <= winPad) {
        hideCursor = true
      }

      if (cursorPointer === 'none') {
        animate = true
      }

      fastdom.mutate(function () {
        if (animate) {
          if (!cursorElem.classList.contains('-flame')) _animateCursor(true)
        } else {
          if (cursorElem.classList.contains('-flame')) _animateCursor(false)
        }
        if (hideCursor) {
          if (cursorElem.classList.contains('-visible')) {
            _hideCursor()
          }

          return false
        } else {
          if (!cursorElem.classList.contains('-visible')) {
            _showCursor()
          }
        } // cursorElem.style['padding'] = elemSize.height + 'px ' + elemSize.width + 'px';

        // add move class and set timeout
        _toggleTimer()
        cursorElem.style[transformProp] = 'translate3d(' + mouseX + 'px,' + mouseY + 'px, 0)'
      })
    })
  }

  function _toggleTimer () {
    window.clearTimeout(cursorTimer)
    if (!cursorElem.classList.contains('-move')) _moveCursor()
    // var millisecBeforeRedirect = 10000;
    cursorTimer = window.setTimeout(function () {
      cursorElem.classList.remove('-move')
    }, 400)
  }

  function _moveCursor () {
    cursorElem.classList.add('-move')
  }

  function _animateCursor (willAnimate) {
    if (willAnimate) {
      cursorElem.classList.add('-flame')
    } else {
      cursorElem.classList.remove('-flame')
    }
  }

  function _showCursor (cursorClasses) {
    // console.log([...cursorClasses]);
    cursorElem.classList.add('-visible') // console.log({
    //     classListShow: [...cursorElem.classList]
    // });
  }

  function _hideCursor () {
    cursorElem.classList.remove('-visible') // console.log({
    //     classListHide: [...cursorElem.classList]
    // });
  }

  return {
    init: init
  }
})()

export default CursorPointer
