import Vue from 'vue'
import App from './App.vue'
import VueJamIcons from 'vue-jam-icons'

Vue.use(VueJamIcons)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
